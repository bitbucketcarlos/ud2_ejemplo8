# Ud2-Ejemplo8
_Ejemplo 8 de la Unidad 2._

Vemos un ejemplo de cómo posicionar elementos relativos a otros en _RelativeLayout_ haciendo uso de los atributos _layout_toEndOf_ y _layout_below_. 

Sólo hemos de fijarnos en el fichero _activity_main.xml_:

```html
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <ImageView
        android:id="@+id/imagen"
        android:layout_width="100dp"
        android:layout_height="100dp"
        android:contentDescription="@string/el_gran_canyon"
        android:scaleType="centerCrop"
        android:src="@drawable/grand_canyon" />

    <TextView
        android:id="@+id/gran_cañon"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_toEndOf="@+id/imagen"
        android:text="@string/el_gran_canyon"
        android:textAppearance="?android:textAppearanceLarge"/>

    <TextView
        android:id="@+id/arizona"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@id/gran_cañon"
        android:layout_toEndOf="@+id/imagen"
        android:text="@string/arizona"
        android:textAppearance="?android:textAppearanceMedium"/>

    <TextView
        android:id="@+id/eeuu"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_below="@id/arizona"
        android:layout_toEndOf="@+id/imagen"
        android:text="@string/eeuu"
        android:textAppearance="?android:textAppearanceSmall"/>

</RelativeLayout>
```
Donde podemos ver que el nombre de la imagen es _grand_canyon_ y se puede encontrar en el directorio _res/drawable_. Notad cómo accedemos al recurso _drawable_ usando el símbolo @.

_Imagen de Pixabay.com (Licencia CC0)_
